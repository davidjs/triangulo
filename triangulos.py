#!/usr/bin/env pythoh3

'''
Program to produce a triangle with numbers
'''

import sys


def line(number: int):
    return str(number) * number



    """Return a string corresponding to the line for number"""


def triangle(number: int):

    if number > 9:
        raise ValueError(" El parametro debe ser menor o igual a 9")
    triangulo = ""
    for i in range(1, number+1):
        triangulo = str(triangulo) + line(i) + '\n'

    return triangulo

    """Return a string corresponding to the triangle for number"""

def main():
    number: int = sys.argv[1]
    text = triangle(int(number))
    print(text)

if __name__ == '__main__':
    main()
